#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

void print_string(unsigned long buffer_length) {
    char message[] = " A secret message, which should not be uncovered!";
    char source[] = { 'A', 'B', 'C' };
    char destination[buffer_length];

    strncpy(destination, source, buffer_length); // Copy non-null terminated source string into larger destination buffer; CWE-170
    printf("Output: %s\n", destination);
}

int main(int argc, char *argv[]) {
  int exit = 0;
  char *end;

  if (argc > 1) {
    unsigned long buffer_length = strtol(argv[1], &end, 10);
    print_string(buffer_length);
  } else {
    printf("Please provide a positive integer as paramater. For example: %s 60\n", argv[0]);
    exit = 1;
  }

  return exit;
}


