#include <stdio.h>
#include <stdlib.h>

void double_free(size_t number_of_memory_blocks) {
  int *memory;

  memory = (int *) malloc(number_of_memory_blocks);

  free(memory);
  free(memory); // Frees memory twice; CWE-415
}

int main() {
  size_t number_of_memory_blocks = 34;
  double_free(number_of_memory_blocks);

  return 0;
}
