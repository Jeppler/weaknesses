#include <stdio.h>
#include <stdlib.h>

void use_after_free(size_t string_size) {
  char *string;

  string = (char *) malloc(string_size);

  free(string);
  

  printf("string after free: %s\n", string); // Tries to print the string after it was freed; CWE-416
}

int main() {
  size_t string_size = 5;
  double_free(string_size);

  return 0;
}
