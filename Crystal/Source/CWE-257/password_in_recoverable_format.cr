require "base64"

user_password = nil

if ARGV.size > 0
    user_password = ARGV[0]
else
    puts "Please enter the password. For example, `\'MyTop$ecretPassword!\'`"
    exit 1
end

password = File.read("password.txt") # CWE-259: hardcoded password in file. Easy to recover. 

if password == user_password 
  puts "Login successful!"
else
  puts "Login unsuccessful!"
end
