def login(user_password)
    password = "$uper$ecret!" # Weakness: hardcoded password; CWE-259

    if password == user_password
    puts "Login successful!"
    else
    puts "Login unsuccessful!"
    end
end

if ARGV.size > 0
    user_password = ARGV[0]
    login(user_password)
else
    puts "Please enter the password. For example, `\'$uper$ecret!\'`"
end