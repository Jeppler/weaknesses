require "base64"

user_password = nil

if ARGV.size > 0
    user_password = ARGV[0]
else
    puts "Please enter the password. For example, `\'$uper$ecret!\'`"
    exit 1
end

b64password = File.read("password.txt")
password = Base64.decode_string(b64password) # CWE-261: password is protected by a weak encoding (base64)

if password == user_password
  puts "Login successful!"
else
  puts "Login unsuccessful!"
end
