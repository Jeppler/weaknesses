require "digest/md5"

def login(password, password_hash_to_check) 
  password_hash = Digest::MD5.hexdigest(password) # Weakness: MD5 is inadequate to protect a login; CWE-326
 
  if password_hash == password_hash_to_check
    puts "You are logged in."
  else
    puts "Wrong password!"
    exit(1)
  end
end

password_hash_to_check = "2c103f2c4ed1e59c0b4e2e01821770fa"

if ARGV.size == 1
  password = ARGV[0]
  
  login(password, password_hash_to_check)
else
  puts "Please enter the password. For example, `\"Password123!\"`"
end
