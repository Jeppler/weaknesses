require "digest/sha1"

def login(password, password_hash_to_check) 
  password_hash = Digest::SHA1.hexdigest(password) # Weakness: SHA1 is inadequate to protect a login; CWE-326
 
  if password_hash == password_hash_to_check
    puts "You are logged in."
  else
    puts "Wrong password!"
    exit(1)
  end
end

password_hash_to_check = "49efef5f70d47adc2db2eb397fbef5f7bc560e29"

if ARGV.size == 1
  password = ARGV[0]
  
  login(password, password_hash_to_check)
else
  puts "Please enter the password. For example, `\"Password123!\"`"
end
