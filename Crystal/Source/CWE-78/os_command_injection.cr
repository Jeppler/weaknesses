if ARGV.size > 0
  command = ARGV.join(" ")
  output = `#{command}` # CWE-78: Os command injection weakness. It executes any given command.
  puts output
else
  puts "Please provide paramaters. For example, `ls -l`."  
end
