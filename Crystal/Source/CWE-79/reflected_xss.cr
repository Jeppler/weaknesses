require "http/server"

def build_html(message)
    html_page = "<DOCTYPE html>
      <html>
        <head>
          <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
          <title>Reflected XSS</title>
        </head>
        <body>
          <h1>Reflected XSS</h1>
          <p>A reflected Cross-Site-Scripting (XSS) example. The abbreviation XSS is used to avoid confusion with Cascading-Style-Sheet (CSS).</p>
          <p>To attack manipulate the <code>message</code> query parameter. Examples:
            <ul>
              <li><a href=\"/?message=Hello\"><code>/?message=Hello</code></a></li>
              <li><a href=\"/?message=<h3>Hello</h3>\"><code>/?message=&lth3&gtHello&lth3&gt</code></a></li>
              <li><a href=\"/?message=<script>alert('hello')</script>\"><code>/?message=&ltscript&gtalert('hello')&lt/script&gt</code></a></li>
              <li><a href=\"http://localhost:8080/?message=<script>window.location.replace('https://wikipedia.org/')</script>\"><code>/?message=&ltscript&gtwindow.location.replace('https://wikipedia.org/')&lt/script&gt</code></a></li>
            </ul>
          </p>
          <span id=\"message\">Message:</span> #{message}
        </body>
      </html>"

    return html_page
end
    
server = HTTP::Server.new do |context|
  query_parameters = context.request.query_params
  message = query_parameters["message"]?
  context.response.content_type = "text/html"
  context.response.print build_html(message)
end

address = server.bind_tcp "0.0.0.0", 8080
puts "Listening on http://#{address}"
server.listen