if ARGV.size > 0
  command = ARGV.join(" ")
  output = `ls #{command}` # Weakness: Executes any given command.; CWE-88
  puts output
else
  puts "Please provide arguments for `ls`. For example, `-l`. To attack use for example, `; ps -ef` to see all processes."
end
