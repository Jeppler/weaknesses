package main

import (
  "crypto/aes"
  "fmt"
  "strings"
  "os"
  "encoding/hex"
)

func main() {
  //key := strings.Repeat("a", 16)
  //key := strings.Repeat("a", 24)
  key := strings.Repeat("a", 32)
  keyBytes := []byte(key)

  plaintext := "The grey donkey and the red fox are best friends. They live together in a stable outside the village and meet every day at dawn."
  plaintextBytes:= []byte(plaintext)
    
  fmt.Printf("[key]\n - length: %d\n - text: %s\n - hex: %s\n\n", len(key), key, hex.EncodeToString(keyBytes))
  fmt.Printf("[original]\n - length: %d\n - text: %s\n - hex: %s\n\n", len(plaintext), plaintext, hex.EncodeToString(plaintextBytes))
  ciphertext := encrypt(keyBytes, plaintextBytes)
  decryptedTextBytes := decrypt(keyBytes, ciphertext)
  decryptedText := string(decryptedTextBytes)
  
  
  fmt.Printf("[ciphertext]\n - length: %d\n - hex: %s\n\n", len(ciphertext), hex.EncodeToString(ciphertext))
  fmt.Printf("[plaintext]\n - length: %d\n - text: %s\n - hex: %s\n\n", len(decryptedText), decryptedText, hex.EncodeToString(decryptedTextBytes))
  fmt.Printf("[bytes]\n - plaintext: \t%08b\n - ciphertext: \t%08b\n\n", decryptedTextBytes, ciphertext)
}

// AES 128 (16 bytes), 192 (24 bytes), 256 (32 bytes)
func encrypt(key []byte, plaintext []byte) []byte {

  cipher, _ := aes.NewCipher([]byte(key))
  
  blockSize := cipher.BlockSize()
  
  if len(plaintext) % blockSize != 0 {
    fmt.Printf("The plaintext needs to be a multiple of the block size of %d\n", blockSize)
    os.Exit(1) 
  }

  ciphertext := make([]byte, len(plaintext))

  for start, end := 0, blockSize; start < len(plaintext); start, end = start + blockSize, end + blockSize {
    cipher.Encrypt(ciphertext[start:end], plaintext[start:end])
  }
    
  return ciphertext
}

func decrypt(key []byte, ciphertext []byte) []byte {

  cipher, _ := aes.NewCipher([]byte(key))
  
  blockSize := cipher.BlockSize()
  
  if len(ciphertext) % blockSize != 0 {
    fmt.Printf("The ciphertext needs to be a multiple of the block size of %d", blockSize)
    os.Exit(1) 
  }
  
  plaintext := make([]byte, len(ciphertext))

  for start, end := 0, blockSize; start < len(ciphertext); start, end = start + blockSize, end + blockSize {
    cipher.Decrypt(plaintext[start:end], ciphertext[start:end])
  }
    
  return plaintext
}
