package main

import (
  "encoding/hex"
	"os"
	"crypto/md5"
	"fmt"
)

func main() {
  const passwordHashToCheck = "2c103f2c4ed1e59c0b4e2e01821770fa"
  
  args := os.Args
  
  if len(args) <= 1 {
    fmt.Println("Please enter the password. For example, `\"Password123!\"`")
    os.Exit(1)
  }
  
  password := args[1]
  
  login(password, passwordHashToCheck)
}

func login(password string, passwordHashToCheck string) {
  passwordInBytes := []byte(password)
  passwordHashBytes := md5.Sum(passwordInBytes) // CWE-328: MD5 is broken
  passwordHash := hex.EncodeToString(passwordHashBytes[:])
  
  if (passwordHash == passwordHashToCheck) {
    fmt.Println("You are logged in.")
  } else {
    fmt.Println("Wrong password!")
    os.Exit(1)
  }
}
