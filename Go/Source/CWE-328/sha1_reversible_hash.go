package main

import (
  "encoding/hex"
	"os"
	"crypto/sha1"
	"fmt"
)

func main() {
  const passwordHashToCheck = "49efef5f70d47adc2db2eb397fbef5f7bc560e29"
  
  args := os.Args
  
  if len(args) <= 1 {
    fmt.Println("Please enter the password. For example, `\"Password123!\"`")
    os.Exit(1)
  }
  
  password := args[1]
  
  login(password, passwordHashToCheck)
}

func login(password string, passwordHashToCheck string) {
  passwordInBytes := []byte(password)
  passwordHashBytes := sha1.Sum(passwordInBytes) // CWE-328: SHA1 is broken
  passwordHash := hex.EncodeToString(passwordHashBytes[:])
  
  if (passwordHash == passwordHashToCheck) {
    fmt.Println("You are logged in.")
  } else {
    fmt.Println("Wrong password!")
    os.Exit(1)
  }
}
