package main

import (
	"os"
	"os/exec"
	"fmt"
)

func main() {
  args := os.Args
  
  if len(args) <= 1 {
    fmt.Println("Please provide an argument. For example: ls -al.")
    os.Exit(1)
  }
  
  command := args[1]
	parameters := args[2:]
	
	out, err := exec.Command(command, parameters...).Output() // CWE-78: OS command injection
	
	if err != nil {
	  fmt.Println(err)
	}
	
	fmt.Println(string(out))
}
