package main

import (
	"os"
	"os/exec"
	"fmt"
)

func main() {
  args := os.Args
  
  if len(args) <= 1 {
    fmt.Println("Please provide an argument. For example: ; ps -ef")
    os.Exit(1)
  }
  
  command := "ls"
	parameters := args[1:]
	
	out, err := exec.Command(command, parameters...).Output() // CWE-88: argument command injection
	
	if err != nil {
	  fmt.Println(err)
	}
	
	fmt.Println(string(out))
}
