public class HardCodedKey {
    // Used command: `echo -n "Password123!" | sha256sum`
    private static String hashedSHA256Key = "a109e36947ad56de1dca1cc49f0ef8ac9ad9a7b1aa0df41fb3c4cb73c1ff01ea"; // CWE-321: Hard coded key

    public static void main(String args[]) {
        if (args.length >= 1) {
            String password = args[0];
            login(password);
        } else {
            System.out.println("Please provide the password. The password is hashed using Sha256.");
            System.out.println("Use this command on Linux: java HardCodedKey $(echo -n 'Password123!' | sha256sum | cut -d ' ' -f 1)");
        }
    }

    public static void login(String password) {
        if (password.equals(hashedSHA256Key)) { // CWE-321: Using hard coded key for comparison
            System.out.println("Login successful");
        } else {
            System.out.println("Wrong password.");
            System.exit(1);
        }
    }
}
