import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {
    public static void main(String args[]) {
        String value = "super$ecret1&1?"; // CWE-321: Could be detected as CWE-321

        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5"); // CWE-327: MD5 is broken
            md5.update(value.getBytes());
            byte[] digest = md5.digest();
            System.out.println(digest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        
	}
	
}