import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA1 {
    public static void main(String args[]) {
        String value = "super$ecret1&1?"; // CWE-321: Could be detected as CWE-321

        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA1"); // CWE-327: SHA1 is considered broken
            sha1.update(value.getBytes());
            byte[] digest = sha1.digest();
            System.out.println(digest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
	}
	
}