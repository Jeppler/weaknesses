/**
 * If the environment variable "GREETINGS" is not set,
 * a Null Pointer Exception will be thrown.
 */
public class NullPointerDereference {
    public static void main(String args[]) {
        String greetings = System.getenv("GREETINGS"); // CWE-476: There is no guarantee, that the environment variable GREETINGS is set
        greetings.trim();

        System.out.println(greetings);
	}
	
}