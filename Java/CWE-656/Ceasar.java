/**
 * The ceasar cipher shifts the text. This is an example of security through obscurity as the ceasar cipher can be cracked. 
 */
public class Ceasar {
    public static void main(String args[]) {
        String message = "";
        char keyAsChar = 'm';
        int key = -1;

        if (args.length == 2) {
            message = args[0];

            String keyAsString = args[1];

            if (keyAsString.length() == 1) {
                keyAsChar = keyAsString.charAt(0);
                key = calculateKey(keyAsChar);
            } else {
                throw new IllegalArgumentException("The key needs to be a single letter");
            }
        } else {
            System.out.println("Please provide a message and a key.");
            System.exit(1);
        }

        String shiftedText = shift(key, message); // CWE-656: The text is not encrypted just transformed (shift operation)

        System.out.println("Encryption key: " + keyAsChar);
        System.out.println("Decryption key: " + (char) ('z' - key));
        System.out.println("Encrypted text: ");
        System.out.println(shiftedText);
    }

    public static int calculateKey(char key) {
        key = Character.toLowerCase(key);

        if (key >= 'a' && key <= 'z') {
            key = (char) (key - 'a' + 1);
        } else {
            throw new IllegalArgumentException("The key is not a letter in the range a-z or A-Z");
        }

        return key;
    }

    public static String shift(int key, String message) {
        char[] text = message.toCharArray();
        char[] shiftedText = new char[text.length];

        for (int index = 0; index < text.length; index++) {
            char character = text[index];

            if (character >= 'a' && character <= 'z') {
                char shiftedCharacter = (char) (((character - 'a' + key) % 26) + 'a');
                shiftedText[index] = shiftedCharacter;
            } else if (character >= 'A' && character <= 'Z') {
                char shiftedCharacter = (char) (((character - 'A' + key) % 26) + 'A');
                shiftedText[index] = shiftedCharacter;
            } else {
                shiftedText[index] = character;
            }
        }

        String shiftedMessage = String.valueOf(shiftedText);

        return shiftedMessage;
    }
}