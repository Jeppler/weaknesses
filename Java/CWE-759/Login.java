import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.nio.charset.StandardCharsets;

/**
 * The main concern of this vulnerability is CWE-759: Use of a One-Way Hash without a Salt.
 * It uses a password without a salt.
 * 
 * Furthermore, this example has several additional weaknesses:
 * - CWE-308: It uses only a single factor for authentication
 * - CWE-309: It uses a password as a primary authentication mechanism
 * - CWE-256: It stores the password as plain text in the source code.
 * - CWE-259: The password is hard-coded into the source code.
 */
public class Login {
    /* 
     * CWE-256: Plain text password, anybody with access to the source code can read it.
     * CWE-259: The password is hard-coded.
     */
    private static String storedPassword = "4SecreT!"; 

    public static void main(String args[]) {
        if (args.length >= 1) {
            String password = args[0];
            login(password);
        } else {
            System.out.println("Please provide `\"" + storedPassword + "\"` as parameter.");
        }
    }

    public static void login(String password) {
        String passwordDigest = getDigest(password);
        String storedPasswordDigest = getDigest(storedPassword);

        if (passwordDigest.equals(storedPasswordDigest)) {
            System.out.println("Login successful!");
        } else {
            System.out.println("Login failed! Please type in the correct password: `\"" + storedPassword + "\"`.");
        }
    }

    private static String getDigest(String password) {
        String hash = null;

        try {
            // Get SHA-512 instance
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");

            // Calculate hash
            messageDigest.update(password.getBytes(StandardCharsets.UTF_8)); // CWE-759: SHA-512 is reversible without a hash.

            // Finalize hash calculation
            byte[] passwordDigest = messageDigest.digest();

            // get base64 encoded hash
            hash = new String(Base64.getEncoder().encode(passwordDigest));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return hash;
    }
}
