# CWE-88: Improper Neutralization of Argument Delimiters in a Command ('Argument Injection')

Abstraction: Base
See: https://cwe.mitre.org/data/definitions/88.html
