import sys

if len(sys.argv) == 2:
  number = float(sys.argv[1])
  
  number = number + (sys.float_info.max*2) # Weakness: Wrap around to inf; CWE-128
  
  print(number)
else:
  print("Please provide a number.")
