import sys

if len(sys.argv) == 2:
  number = float(sys.argv[1])
  
  number = number + sys.float_info.max # Weakness: Wrap around to 1.7976931348623157e+308 and will stay at the value. Except if you add 1.7976931348623157e+292, then it will be inf; CWE-128
  
  print(number)
else:
  print("Please provide a number.")
