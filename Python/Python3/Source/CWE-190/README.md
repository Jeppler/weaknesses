# CWE-190: Integer Overflow or Wraparound

As of Python 3 it is not possible to overflow an integer. Python 3 uses a big integer representation which can grow.

~~~
>>> print(sys.maxsize+sys.maxsize)
18446744073709551614
>>> type(sys.maxsize+sys.maxsize)
<class 'int'>
~~~
