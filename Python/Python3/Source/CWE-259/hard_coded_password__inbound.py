import sys

password = "letMeIn123!"

def login(user_password):
  if (password == user_password):
    print("You are logged in.")
  else:
    print("Login failed")

if __name__ == "__main__":
  if len(sys.argv) == 2:
    user_password = sys.argv[1]
    login(user_password)
  else:
    message = "Please provide a password as argument. Use: {}".format(password)
    print(message)
