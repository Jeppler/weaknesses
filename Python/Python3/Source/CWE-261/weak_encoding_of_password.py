import base64

with open('password.txt') as reader:
  b64password = reader.read()
  password = base64.b64decode(b64password).decode('UTF-8').rstrip() # Weakness: password encoded using base64; CWE-261
  
  if "$uper$ecret!" == password: # Weakness: hardcoded password; CWE-259
    print("Login successful!")
  else:
    print("Login unsuccessful!")
