import sys
import hashlib

password_hash_to_check = "2c103f2c4ed1e59c0b4e2e01821770fa"

def login(password):
  password_hash = hashlib.md5(password.encode()).hexdigest() # Weakness: MD5 is inadequate to protect a login; CWE-326
 
  if password_hash == password_hash_to_check:
    print("You are logged in.")
  else:
    print("Wrong password!")
    exit(1)

if __name__ == "__main__":
  if len(sys.argv) == 2:
    password = sys.argv[1]
    
    login(password)
  else:
    print('Please enter the password. For example, `"Password123!"`')
