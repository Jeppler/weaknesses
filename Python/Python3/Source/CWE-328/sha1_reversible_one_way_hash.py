import sys
import hashlib

password_hash_to_check = "49efef5f70d47adc2db2eb397fbef5f7bc560e29"

def login(password):
  password_hash = hashlib.sha1(password.encode()).hexdigest() # Weakness: Sha1 is inadequate to protect a login; CWE-326
 
  if password_hash == password_hash_to_check:
    print("You are logged in.")
  else:
    print("Wrong password!")
    exit(1)

if __name__ == "__main__":
  if len(sys.argv) == 2:
    password = sys.argv[1]
    
    login(password)
  else:
    print('Please enter the password. For example, `"Password123!"`')

