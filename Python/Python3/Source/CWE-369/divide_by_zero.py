import sys

def divide(a, b):
  return (a / b) # Weakness: causes a division by zero exception; CWE-369

if len(sys.argv) == 3:
  number_a = int(sys.argv[1])
  number_b = int(sys.argv[2])
  
  number_b = number_b % number_b # The result will always be zero.
  
  divide(number_a, number_b)
else:
  print("Please provide two numbers to divide.")
