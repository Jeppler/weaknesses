# CWE-78: Improper Neutralization of Special Elements used in an OS Command ('OS Command Injection')

Abstraction: Base
See: https://cwe.mitre.org/data/definitions/78.html
