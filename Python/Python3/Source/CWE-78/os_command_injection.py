import sys
import os

if len(sys.argv) > 1:
  command = sys.argv[1:]
  command = ' '.join(command)
  
  os.system(command) # Weakness: Executes any given command.; CWE-78
else:
  print("Please provide a command to run. For example, `ls` or `ps -ef`.")
