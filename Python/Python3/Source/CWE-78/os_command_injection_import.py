import sys

if len(sys.argv) > 1:
  # same as import os
  my_os = __import__('os', globals(), locals(), [], 0)

  command = sys.argv[1:]
  command = ' '.join(command)
  
  my_os.system(command) # Weakness: Executes any given command.; CWE-78
else:
  print("Please provide a command to run. For example, `ls` or `ps -ef`.")
