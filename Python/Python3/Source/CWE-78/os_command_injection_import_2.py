import sys
from importlib import import_module

if len(sys.argv) > 1:
  # same as import os
  my_os = import_module('os', package=None)

  command = sys.argv[1:]
  command = ' '.join(command)
  
  my_os.system(command) # Weakness: Executes any given command.; CWE-78
else:
  print("Please provide a command to run. For example, `ls` or `ps -ef`.")
