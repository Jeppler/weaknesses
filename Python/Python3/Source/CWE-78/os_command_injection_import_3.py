import sys

if len(sys.argv) > 1:
  # same as from os import system
  _temp = __import__('os', globals(), locals(), ['system'], 0)
  # assign os.system to my_system
  my_system = _temp.system

  command = sys.argv[1:]
  command = ' '.join(command)
  
  my_system(command) # Weakness: Executes any given command.; CWE-78
else:
  print("Please provide a command to run. For example, `ls` or `ps -ef`.")
