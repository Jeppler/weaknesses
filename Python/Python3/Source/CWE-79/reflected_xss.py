from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs, unquote

hostname = "0.0.0.0"
port = 8080

class WebApplication(BaseHTTPRequestHandler):
  def do_GET(self):
    message = self._get_query("message")
    html_page = self._build_html(message)
    self.send_response(200)
    self.send_header("Content-type", "text/html")
    self.end_headers()
    self.wfile.write(bytes(html_page, "utf-8"))
    
  def _build_html(self, message):
    html_page = """<DOCTYPE html>
      <html>
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <title>Reflected XSS</title>
        </head>
        <body>
          <h1>Reflected XSS</h1>
          <p>A reflected Cross-Site-Scripting (XSS) example. The abbreviation XSS is used to avoid confusion with Cascading-Style-Sheet (CSS).</p>
          <p>To attack manipulate the <code>message</code> query parameter. Examples:
            <ul>
              <li><a href="/?message=Hello"><code>/?message=Hello</code></a></li>
              <li><a href="/?message=<h3>Hello</h3>"><code>/?message=&lth3&gtHello&lth3&gt</code></a></li>
              <li><a href="/?message=<script>alert('hello')</script>"><code>/?message=&ltscript&gtalert('hello')&lt/script&gt</code></a></li>
              <li><a href="http://localhost:8080/?message=<script>window.location.replace('https://wikipedia.org/')</script>"><code>/?message=&ltscript&gtwindow.location.replace('https://wikipedia.org/')&lt/script&gt</code></a></li>
            </ul>
          </p>
          <span id="message">Message:</span> {}
        </body>
      </html>""".format(message)

    return html_page
    
  def _get_query(self, name):
    query = urlparse(self.path).query
    query_components = parse_qs(query)
    value = query_components.get(name, None) # returns a list
    value = str(value) # converts the list to a string
    value = value.replace("['","").replace("']", "") # replaces the [' and '] from the string
   
    return value

if __name__ == "__main__":        
  web_server = HTTPServer((hostname, port), WebApplication)
  print("Server started http://{}:{}".format(hostname, port))
  print("Press CTRL+C to stop server.")

  try:
    web_server.serve_forever()
  except KeyboardInterrupt:
    web_server.server_close()
    print("Server stopped.")


