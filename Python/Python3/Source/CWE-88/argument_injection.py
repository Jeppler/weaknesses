import sys
import os

if len(sys.argv) > 1:
  arguments = sys.argv[1:]
  arguments = ' '.join(arguments)
  
  command = "ls {}".format(arguments)
  
  os.system(command) # Weakness: Executes any given command.; CWE-88
else:
  print("Please provide arguments for `ls`. For example, `-l`. To attack use for example, `; ps -ef` to see all processes.")
