import sys
import sqlite3

create_user_table = '''
CREATE TABLE user (
  uid INTEGER PRIMARY KEY,
  name TEXT,
  password TEXT
)
'''

insert_users = '''
INSERT INTO user VALUES 
(1, 'Peter', 'secret123'),
(2, 'Admin', 'password!'),
(3, 'HR', 'humanXyZ!')
'''
  
def search_user(name):
  connection = sqlite3.connect("file::memory:?cache=shared")
    
  cursor = connection.cursor()
  cursor.execute(create_user_table)
  cursor.execute(insert_users)
  cursor.execute("SELECT * FROM user where name = '" + name + "';")
  print(cursor.fetchall())

if len(sys.argv) == 2:
  name = sys.argv[1]
  search_user(name)
else:
  print("Please provide a name to search for. For example, Peter. To attack use \"' AND 1 OR 1; --\".")
  
  
