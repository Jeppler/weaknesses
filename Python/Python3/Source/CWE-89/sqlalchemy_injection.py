#!/usr/bin/env python3

import sys
import sqlite3
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select

Base = declarative_base()

# create in-memory database. See: https://docs.sqlalchemy.org/en/13/dialects/sqlite.html#dialect-sqlite-pysqlite-connect
#engine = create_engine("sqlite://")

# to see all SQL statements uncomment this line
engine = create_engine("sqlite://", echo = True)

class User(Base):
  __tablename__ = 'user'
  id = Column(Integer, primary_key = True)
  name = Column(String)
  password = Column(String)
 
def initialize():
  Base.metadata.create_all(engine)
  
  Session = sessionmaker(bind = engine)
  session = Session()
  
  user1 = User(name = 'Peter', password = 'secret123')
  user2 = User(name = 'Admin', password = 'password!')
  user3 = User(name = 'HR', password = 'humanXyZ!')
  
  session.add(user1)
  session.add(user2)
  session.add(user3)
  
  session.commit()
  
def search_user(name):
  with engine.connect() as connection:
    result = connection.execute("SELECT * FROM user where name = '" + name + "';")

    for row in result:
        print(row)

if len(sys.argv) == 2:
  name = sys.argv[1]
  initialize()
  search_user(name)
else:
  print("Please provide a name to search for. For example, Peter. To attack use \"' AND 1 OR 1; --\".")
  
  
