# CWE-94: Improper Control of Generation of Code ('Code Injection')

Abstraction: Base
See: https://cwe.mitre.org/data/definitions/94.html

OWASP: https://owasp.org/www-community/attacks/Code_Injection
