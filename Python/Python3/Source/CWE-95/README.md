# CWE-95: Improper Neutralization of Directives in Dynamically Evaluated Code ('Eval Injection')

Abstraction: Variant
See: https://cwe.mitre.org/data/definitions/95.html
