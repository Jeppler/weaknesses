import sys

if len(sys.argv) > 1:
  command = sys.argv[1:]
  command = ' '.join(command)
  
  result = eval(command) # Weakness: Evaluates any given command.; CWE-95
  print(result)
else:
  print("Please provide a Python command to run. For example, `'1+2'` or `'print(\"a*5\")'`.")
