import sys

if len(sys.argv) > 1:
  command = sys.argv[1:]
  command = ' '.join(command)
  
  result = exec(command) # Weakness: Evaluates any given command.; CWE-95
else:
  print("Please provide a Python command to run. For example, `'print(\"a\"*5)'` or `'import os as o; o.system(\"ls -l\")'`.")
