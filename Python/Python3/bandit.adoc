= Bandit

Build container:

----
docker-compose -f bandit.yaml up --build
----

Enter container:

----
docker exec -it bandit bash
----

Run:

----
bandit -r .
----
