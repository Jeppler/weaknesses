#!/usr/bin/env ruby

if ARGV.length > 0
  command = ARGV.join(" ")
  output = `#{command}` # Weakness: Executes any given command.; CWE-78
  puts output
else
  puts "Please provide paramaters. For example, `ls -l`."  
end
