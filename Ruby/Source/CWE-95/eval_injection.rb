#!/usr/bin/env ruby

if ARGV.length > 0
  command = ARGV.join(" ")
  output = eval(command) # Weakness: Evaluates any given command.; CWE-95
  puts output
else
  puts "Please provide paramaters. For example, '`ls`' or 'system(\"date\")'."  
end
