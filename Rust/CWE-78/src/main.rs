use std::process::Command;
use std::env;

fn main() {
    let args: Vec<String> = env::args().skip(1).collect();

    if args.len() > 1 {
        let command = args.join(" ");
        println!("Command: {}", command);

        let output = Command::new("sh").arg("-c").arg(command).output().expect("command failed"); // CWE-78: OS injection

        println!("stdout: {}", String::from_utf8_lossy(&output.stdout));
        println!("stderr: {}", String::from_utf8_lossy(&output.stderr));
    } else {
        println!("Please provide an argument.");
    }
}